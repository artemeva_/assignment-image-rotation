#ifndef TRANSFORMATION_H
#define TRANSFORMATION_H

#include "image.h"

void get_pixel(struct image* img, struct image const* source, uint64_t x, uint64_t y);

struct image rotate( const struct image* source);

#endif
