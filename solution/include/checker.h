#ifndef CHECKER_H
#define CHECKER_H

#include "image.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>




enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE = -1,
  READ_INVALID_BITS = -1,
  READ_INVALID_HEADER = -1,
  READ_ERROR = -1
  };

uint64_t getpadding(uint64_t width);

struct bmp_header new_header(struct image const* img, const uint64_t padding);

enum read_status header_check(const uint64_t s_write, struct bmp_header header, struct image* img);

enum read_status from_bmp( FILE* in, struct image* img );


enum  write_status  {
  WRITE_OK = 0,
  WRITE_ERROR
};

enum write_status to_bmp( FILE* out, struct image const* img );




#endif

