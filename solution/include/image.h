#ifndef IMAGE_H
#define IMAGE_H

#include  <stdint.h>

struct image {
  uint64_t width, height;
  struct pixel* data;
};

struct pixel { 
  uint8_t b, g, r; 
};


struct image image_make(uint64_t width, uint64_t height);

void image_delete(struct image *image);

#endif

