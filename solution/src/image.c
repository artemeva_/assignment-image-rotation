#include "../include/image.h"
#include <stdlib.h>
#include <string.h>

struct image image_make(uint64_t width, uint64_t height) {
    struct image image;

    image.width = width;
    image.height = height;
    image.data = malloc(sizeof(struct pixel) * width * height);

    return image;
}

void image_delete(struct image *image) {
    free(image->data);
}
