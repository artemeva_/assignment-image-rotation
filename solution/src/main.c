#include "../include/bmp.h"
#include "../include/checker.h"
#include "../include/image.h"
#include "../include/transformation.h"
#include <stdio.h>
#include <stdlib.h>

#define number_of_arguments 2
#define RB 1
#define WB 2

int main( int argc, char** argv ) {

    if (argc < number_of_arguments){
        fprintf(stderr, "wrong amount");
    }

    FILE *in_f= fopen(argv[RB], "rb");
    FILE *out_f = fopen(argv[WB], "wb");
    if (in_f  && out_f){
    
    struct image *img = malloc(sizeof(struct image));
 

    enum read_status read_status_cur = from_bmp(in_f, img);
    if (read_status_cur != READ_OK){
        fprintf(stderr, "file not read");
        free(img);
        return 1;
    }
    
    struct image *transform_img = malloc(sizeof(struct image));  
	*transform_img = rotate( img );
    fprintf(stderr, "success rotated image");

    enum write_status write_status_cur =  to_bmp(out_f, transform_img);
    if (write_status_cur != WRITE_OK){
        fprintf(stderr, "file not write");
        free(img);
        free(transform_img);
        return 1;
    }

    fclose(in_f);
    fclose(out_f);
    image_delete(img);
    image_delete(transform_img);
    free(img);
    free(transform_img);

    }else{
        fprintf(stderr, "failed to open file");
        return 1;
        }
    

  
    return 0;
}


