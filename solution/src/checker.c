#include "../include/checker.h"
#include "../include/bmp.h"
#include "../include/image.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define header_bfType 0x4d42
#define header_bOffBits 54
#define header_biSize 40
#define header_biPlanes 1
#define header_biBitCount 24
#define header_biCompression 0
#define header_bfReserved 0


uint64_t getpadding(uint64_t width){
    return (4 -((width * 3) % 4)) % 4;
}

struct bmp_header new_header(struct image const* img, const uint64_t padding){
    struct bmp_header header = {0};
        header.bfType = header_bfType,
		header.bOffBits = header_bOffBits,
		header.biSize = header_biSize,
		header.biWidth = img->width,
		header.biHeight = img->height,
		header.biPlanes = header_biPlanes,
		header.biBitCount = header_biBitCount,
		header.biCompression = header_biCompression,
        header.bfReserved = header_bfReserved,
		header.biSizeImage = (img->width * sizeof(struct pixel) + padding) * img->height,
		header.bfileSize = (img->width * sizeof(struct pixel) + padding) * img->height + sizeof(struct bmp_header);
    return header;
}

enum read_status header_check(const uint64_t s_write, struct bmp_header header, struct image* img){
    if (s_write!=1) {
        fprintf(stderr, "read invalid header");
        free(img); 
        return READ_INVALID_HEADER;
        }
    if (header.bfType != header_bfType) {
        fprintf(stderr, "read invalid signature");
        free(img);
         return READ_INVALID_SIGNATURE;
         }
    if (header.biBitCount != header_biBitCount) {
        fprintf(stderr, "read invalid bits");
        free(img); 
        return READ_INVALID_BITS;
        }
    return READ_OK;
}


enum read_status from_bmp( FILE* in, struct image* img ){
    struct bmp_header header = {0};
    const uint64_t s_write = fread(&header, sizeof(struct bmp_header), 1, in);
    enum read_status cur_status_write_header = header_check(s_write,header,img);
    if (cur_status_write_header!=READ_OK){
        fprintf(stderr, "read header error");
        return READ_ERROR;
    }

    *img = image_make(header.biWidth, header.biHeight);
    uint64_t padding = getpadding(img->width);
    for(int64_t i = 0; i < img->height; i++){
        if (fread(img->data + img->width * i, sizeof(struct pixel), img->width, in )!= img->width){
            fprintf(stderr, "read error");
            return READ_ERROR;
        }
        if (!fseek(in, (int64_t) padding, SEEK_CUR)){ 
                fprintf(stderr, "fseek error");
        }
    }
    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ){
    uint64_t padding = getpadding(img->width);
     struct bmp_header header = new_header(img, padding);

     if (fwrite(&header, sizeof(struct bmp_header), 1, out) !=1){
        fprintf(stderr, "write error");
        return WRITE_ERROR;
    }
    struct pixel *trash = img->data;
    for(int64_t i = 0; i < img->height; i++){
        if (fwrite(img->data + i*img->width, sizeof(struct pixel) * img->width, 1, out )!= 1){
            fprintf(stderr, "write error");
            return WRITE_ERROR;
        }
        if ( !fwrite(  trash, padding, 1, out ) && padding != 0 ) {
            fprintf(stderr, "write error");
			return WRITE_ERROR;
		}

    }
    return WRITE_OK;
}




