#include "../include/transformation.h"
#include "../include/image.h"
#include <stdio.h>


void get_pixel(struct image* img, struct image const* source, uint64_t x, uint64_t y) {
    img->data[y * img->width + x] = source->data[ source->height * source->width - source->width * x - (source->width-y)];
}


struct image rotate( const struct image* source ){
    struct image transform_img = image_make(source->height, source->width);
    for (int64_t i = 0; i<transform_img.width; i++) {
        for (int64_t j = 0; j < transform_img.height; j++) {
            get_pixel(&transform_img,source,i,j);

        }
    }
    return transform_img;
}
